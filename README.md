<h1>💁‍♂️ 투데이 링크 서비스 소개</h1>
무료로 참여할 수 있는 다양한 이벤트 정보를 한 곳에 모아놓은 웹 서비스입니다.

사용자들은 **투데이 링크** 서비스를 통해 다양한 이벤트 정보를 확인하고 링크 접속을 통해 이벤트에 참여할 수 있습니다.


# 💻 배포 <a href="https://todayslink.net" target="_blank">https://www.todayslink.net</a>


# 🔍 테스트 계정

`아이디 : test123`

`비밀번호 : @xptmxm12`


# ⚒️ 주요 기능 및 화면
<details>
<summary><h4>메인 화면 & 필터링</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (12)](https://user-images.githubusercontent.com/43470398/222310026-3da760b0-3734-4581-913d-5e7ad6c3c2a7.gif)

</div>
</details>

<details>
<summary><h4>이벤트 상세 페이지</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (13)](https://user-images.githubusercontent.com/43470398/222310802-305bf522-bc14-44e2-a178-80723d6a583d.gif)

</div>
</details>


<details>
<summary><h4>회원가입 및 이메일 인증</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif](https://user-images.githubusercontent.com/43470398/221482825-a49ea4e5-a04d-46fc-8033-171ee02210e9.gif)

</div>
</details>

<details>
<summary><h4>로그인</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (1)](https://user-images.githubusercontent.com/43470398/221486561-8fb47d32-8675-42f7-942d-be2f7d4dcc09.gif)

</div>
</details>


<details>
<summary><h4>마이페이지 - 비밀번호 변경</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (2)](https://user-images.githubusercontent.com/43470398/221487752-030a4f6c-c9d1-4da3-ade6-0921e9c93418.gif)

</div>
</details>

<details>
<summary><h4>마이페이지 - 참여한 이벤트 목록</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (19)](https://user-images.githubusercontent.com/43470398/227782173-f1bb79d2-98c0-487b-841c-5c54ddf4922b.gif)

</div>
</details>

<details>
<summary><h4>Q&A 페이지</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (3)](https://user-images.githubusercontent.com/43470398/221488437-da49a085-3311-4f51-bff4-20e3691efd59.gif)

</div>
</details>

<details>
<summary><h4>자주 묻는 질문 페이지</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (4)](https://user-images.githubusercontent.com/43470398/221489852-650ffb97-89f2-42c7-abe9-7c4c3ee64e7a.gif)

</div>
</details>

<details>
<summary><h4>반응형 디자인</h4></summary>
<div markdown="1">

![ezgif com-video-to-gif (5)](https://user-images.githubusercontent.com/43470398/221491874-929e3a84-7da5-4902-912c-50b214ce5679.gif)

</div>
</details>


# 📊 최적화 기록

(2023.04.13) **React.memo, useCallback, useMemo를 통한 TagList 컴포넌트 렌더링 최적화 ([코드 보기](https://github.com/tpgus/TodaysLink/blob/main/components/filter/TagList.tsx))**

(2023.04.15) **이벤트 버블링 및 이벤트 위임을 통한 최적화 ([블로그 기록](https://velog.io/@tpgus758/%EC%9D%B4%EB%B2%A4%ED%8A%B8-%EB%B2%84%EB%B8%94%EB%A7%81%EA%B3%BC-%EC%9D%B4%EB%B2%A4%ED%8A%B8-%EC%9C%84%EC%9E%84%EC%9D%84-%ED%86%B5%ED%95%9C-%EC%BD%94%EB%93%9C-%EA%B0%9C%EC%84%A0))**

# 📌 Todo List
- (04.16 기록) 이벤트 상세 페이지 서버 사이드 함수를 `getStaticProps`에서 `getServerSideProps`로 변경 후 느려진 문제 수정하기


# ⚙️ 프로젝트 기술 스택

|기술|버전|
|---|---|
|<img src="https://img.shields.io/badge/react-61DAFB?style=for-the-badge&logo=react&logoColor=black">|v18.2.0|
|<img src="https://img.shields.io/badge/Next.js-000000?style=for-the-badge&logo=Next.js&logoColor=white">|v13.1.1|
|<img src="https://img.shields.io/badge/typescript-3178C6?style=for-the-badge&logo=typescript&logoColor=white">|v4.9.4|
|<img src="https://img.shields.io/badge/redux-764ABC?style=for-the-badge&logo=redux&logoColor=white">|v8.0.5|
|<img src="https://img.shields.io/badge/styled components-DB7093?style=for-the-badge&logo=styled-components&logoColor=white">|v5.3.6|
|<img src="https://img.shields.io/badge/firebase-FFCA28?style=for-the-badge&logo=firebase&logoColor=black">|v9.17.1|
|<img src="https://img.shields.io/badge/vercel-000000?style=for-the-badge&logo=vercel&logoColor=white">|배포|


# 🔗 WiKi 문서 [바로가기](https://github.com/tpgus/TodaysLink-client/wiki)
